#include "../include/NGhostWatcher.h"

// -----------------------
// namespace NGhostWatcher
// -----------------------

/**
 * The entry point
 *
 * @param argc
 * 		The arguments count
 * @param argv
 * 		The arguments vector
 *
 * @return EXIT_SUCCESS if ok, EXIT_FAILURE else
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Ghost watcher
	NGhostWatcher *ghostWatcher;

	// Init NLib
	NLib_Initialiser( NULL );

	// Build watcher
	if( !( ghostWatcher = NGhostWatcher_NGhostWatcher_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy NLib
		NLib_Detruire( );

		// Quit
		return EXIT_FAILURE;
	}

	// Display header
	NDeviceCommon_DisplayHeader( "Watchdog",
		NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetNetworkListeningPort( NGhostWatcher_NGhostWatcher_GetConfiguration( ghostWatcher ) ),
		NGhostWatcher_NGhostWatcher_GetIP( ghostWatcher ) );

	// Main loop
	while( NGhostWatcher_NGhostWatcher_IsRunning( ghostWatcher ) )
		// Wait
		NLib_Temps_Attendre( 16 );

	// Destroy watcher
	NGhostWatcher_NGhostWatcher_Destroy( &ghostWatcher );

	// Destroy NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}
