#define NGHOSTWATCHER_SERVICE_NGHOSTWATCHERSERVICE_INTERNE
#include "../../include/NGhostWatcher.h"

// -------------------------------------------------
// enum NGhostWatcher::Service::NGhostWatcherService
// -------------------------------------------------

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param requestType
 * 		The request type
 *
 * @return the service type
 */
NGhostWatcherService NGhostWatcher_Service_NGhostWatcherService_FindService( const char *requestedElement,
	NU32 *cursor,
	NTypeRequeteHTTP requestType )
{
	// Output
	__OUTPUT NGhostWatcherService out = (NGhostWatcherService)0;

	// Look for
	for( ; out < NGHOST_WATCHER_SERVICES; out++ )
		// Check
		if( NLib_Chaine_EstChaineCommencePar( requestedElement,
			NGhostWatcherServiceName[ out ] )
			&& requestType == NGhostWatcherServiceType[ out ] )
		{
			// Set cursor
			*cursor = (NU32)strlen( NGhostWatcherServiceName[ out ] );

			// OK
			return out;
		}

	// Not found
	return NGHOST_WATCHER_SERVICES;
}
