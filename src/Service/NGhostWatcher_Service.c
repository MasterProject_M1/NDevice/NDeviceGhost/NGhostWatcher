#include "../../include/NGhostWatcher.h"

// --------------------------------
// namespace NGhostWatcher::Service
// --------------------------------

/**
 * Receive callback
 *
 * @param request
 * 		The request
 * @param client
 * 		The requesting client
 *
 * @return the http response
 */
__CALLBACK __ALLOC NReponseHTTP *NGhostWatcher_Service_ReceiveCallback( const NRequeteHTTP *request,
	const NClientServeur *client )
{
	// HTTP server
	NHTTPServeur *httpServer;

	// Ghost watcher
	NGhostWatcher *ghostWatcher;

	// Ghost common
	NGhostCommon *ghostCommon;

	// Is authenticated
	NBOOL isAuthenticated;

	// Cursor in requested element
	NU32 cursor = 0;

	// Response
	__OUTPUT NReponseHTTP *response;

	// Service type
	NGhostCommonService serviceType;

	// Ghost common
	if( !( httpServer = (NHTTPServeur*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client ) )
		|| !( ghostWatcher = NHTTP_Serveur_NHTTPServeur_ObtenirDataUtilisateur( httpServer ) )
		|| !( ghostCommon = (NGhostCommon*)NGhostWatcher_NGhostWatcher_GetGhostCommon( ghostWatcher ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NULL;
	}

	// Basic service
	if( ( response = NGhostCommon_Service_ProcessBasicService( request,
		ghostCommon,
		&isAuthenticated,
		&serviceType,
		&cursor,
		client,
		NGhostCommon_NGhostCommon_GetIP( ghostCommon ) ) ) != NULL )
		return response;

	// Check if we are authenticated
	if( !isAuthenticated )
	{
		// Notify
		NOTIFIER_AVERTISSEMENT( NERREUR_AUTH_FAILED );

		// Quit
		return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_UNAUTHORIZED_MESSAGE,
			NHTTP_CODE_401_UNAUTHORIZED,
			NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );
	}

	// Process service
	cursor = 0;
	switch( NGhostWatcher_Service_NGhostWatcherService_FindService( NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElementInitial( request ),
		&cursor,
		NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirType( request ) ) )
	{
		case NGHOST_WATCHER_SERVICE_EVENT_GET:
		case NGHOST_WATCHER_SERVICE_EVENT_POST:
		case NGHOST_WATCHER_SERVICE_EVENT_DELETE:
			return NGhostCommon_Event_NGhostEventList_ProcessRESTRequest( NGhostWatcher_NGhostWatcher_GetEventList( ghostWatcher ),
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirElementInitial( request ),
				&cursor,
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirType( request ),
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirData( request ),
				NHTTP_Commun_HTTP_Traitement_Requete_NRequeteHTTP_ObtenirTailleData( request ),
				client );

		default:
			return NHTTP_Commun_HTTP_Traitement_Reponse_Usine_CreerPacketMessageSimple( NDEVICE_COMMON_TYPE_GHOST_BAD_REQUEST_MESSAGE,
				NHTTP_CODE_400_BAD_REQUEST,
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );
	}
}
