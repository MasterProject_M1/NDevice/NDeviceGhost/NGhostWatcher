#define NGHOSTWATCHER_CONFIGURATION_NGHOSTWATCHERCONFIGURATIONPROPERTY_INTERNE
#include "../../include/NGhostWatcher.h"

// ---------------------------------------------------------------------
// enum NGhostWatcher::Configuration::NGhostWatcherConfigurationProperty
// ---------------------------------------------------------------------

/**
 * Get property name
 *
 * @param property
 * 		The property
 *
 * @return the property name
 */
const char *NGhostWatcher_Configuration_NGhostWatcherConfigurationProperty_GetName( NGhostWatcherConfigurationProperty property )
{
	return NGhostWatcherConfigurationPropertyName[ property ];
}

/**
 * Generate properties list
 *
 * @return the properties list
 */
__ALLOC char **NGhostWatcher_Configuration_NGhostWatcherConfigurationProperty_BuildPropertyList( void )
{
	// Output
	char **out;

	// Iterator
	NU32 i;

	// Allocate memory
	if( !( out = calloc( NGHOST_WATCHER_CONFIGURATION_PROPERTIES,
		sizeof( char* ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Copy addresses
	for( i = 0; i < NGHOST_WATCHER_CONFIGURATION_PROPERTIES; i++ )
		out[ i ] = (char*)NGhostWatcherConfigurationPropertyName[ i ];

	// OK
	return out;
}

