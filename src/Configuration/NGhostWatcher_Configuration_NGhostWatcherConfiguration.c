#include "../../include/NGhostWatcher.h"

// ---------------------------------------------------------------
// struct NGhostWatcher::Configuration::NGhostWatcherConfiguration
// ---------------------------------------------------------------

/**
 * Save configuration
 *
 * @param this
 * 		This instance
 * @param configurationFilePath
 * 		The configuration file path
 *
 * @return if the operation succeeded
 */
__PRIVATE NBOOL NGhostWatcher_Configuration_NGhostWatcherConfiguration_Save( const NGhostWatcherConfiguration *this,
	const char *configurationFilePath )
{
	// File
	NFichierTexte *file;

	// Export
	char *export;

	// Open file
	if( !( file = NLib_Fichier_NFichierTexte_ConstruireEcriture( configurationFilePath,
		NTRUE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OVERRIDE );

		// Quit
		return NFALSE;
	}

	// Device
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"[Device]\n" );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		NGhostWatcher_Configuration_NGhostWatcherConfigurationProperty_GetName( NGHOST_WATCHER_CONFIGURATION_PROPERTY_DEVICE_NAME ) );
	NLib_Fichier_NFichierTexte_Ecrire6( file,
		' ' );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		this->m_deviceName );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"\n\n" );

	// Authentication
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"[Authentication]\n" );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		NGhostWatcher_Configuration_NGhostWatcherConfigurationProperty_GetName( NGHOST_WATCHER_CONFIGURATION_PROPERTY_AUTHENTICATION_USERNAME ) );
	NLib_Fichier_NFichierTexte_Ecrire6( file,
		' ' );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		this->m_authenticationUsername );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"\n" );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		NGhostWatcher_Configuration_NGhostWatcherConfigurationProperty_GetName( NGHOST_WATCHER_CONFIGURATION_PROPERTY_AUTHENTICATION_PASSWORD ) );
	NLib_Fichier_NFichierTexte_Ecrire6( file,
		' ' );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		this->m_authenticationPassword );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"\n" );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		NGhostWatcher_Configuration_NGhostWatcherConfigurationProperty_GetName( NGHOST_WATCHER_CONFIGURATION_PROPERTY_AUTHENTICATION_AUTHORIZED_TOKEN_IP ) );
	NLib_Fichier_NFichierTexte_Ecrire6( file,
		' ' );
	if( ( export = NGhostCommon_Token_NGhostTrustedTokenIPList_Export( this->m_authenticationTrustedIPList ) ) != NULL )
	{
		NLib_Fichier_NFichierTexte_Ecrire3( file,
			export );
		NFREE( export );
	}
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"\n\n" );

	// Network
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"[Network]\n" );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		NGhostWatcher_Configuration_NGhostWatcherConfigurationProperty_GetName( NGHOST_WATCHER_CONFIGURATION_PROPERTY_NETWORK_LISTENING_INTERFACE ) );
	NLib_Fichier_NFichierTexte_Ecrire6( file,
		' ' );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		this->m_networkListeningInterface );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"\n" );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		NGhostWatcher_Configuration_NGhostWatcherConfigurationProperty_GetName( NGHOST_WATCHER_CONFIGURATION_PROPERTY_NETWORK_LISTENING_PORT ) );
	NLib_Fichier_NFichierTexte_Ecrire6( file,
		' ' );
	NLib_Fichier_NFichierTexte_Ecrire( file,
		this->m_networkListeningPort );
	NLib_Fichier_NFichierTexte_Ecrire3( file,
		"\n\n" );

	// Close file
	NLib_Fichier_NFichierTexte_Detruire( &file );

	// OK
	return NTRUE;
}

/**
 * Build watcher configuration
 *
 * @param configurationFilePath
 * 		The configuration file path
 *
 * @return the watcher configuration instance
 */
__ALLOC NGhostWatcherConfiguration *NGhostWatcher_Configuration_NGhostWatcherConfiguration_Build( const char *configurationFilePath )
{
	// Output
	__OUTPUT NGhostWatcherConfiguration *out;

	// Key list
	char **keyList;

	// File
	NFichierClef *file;

	// Build key list
	if( !( keyList = NGhostWatcher_Configuration_NGhostWatcherConfigurationProperty_BuildPropertyList( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Load file
	if( !( file = NLib_Fichier_Clef_NFichierClef_Construire( configurationFilePath,
		(const char**)keyList,
		NGHOST_WATCHER_CONFIGURATION_PROPERTIES,
		NTRUE ) ) )
	{
		// Build default configuration
		if( !( out = NGhostWatcher_Configuration_NGhostWatcherConfiguration_Build2( ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Free
			NFREE( keyList );

			// Quit
			return NULL;
		}

		// Free
		NFREE( keyList );

		// Save
		NGhostWatcher_Configuration_NGhostWatcherConfiguration_Save( out,
			configurationFilePath );

		// OK
		return out;
	}

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostWatcherConfiguration ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Destroy file
		NLib_Fichier_Clef_NFichierClef_Detruire( &file );

		// Quit
		return NULL;
	}

	// Free
	NFREE( keyList );

	// Get values
	if( !( out->m_deviceName = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( file,
		NGHOST_WATCHER_CONFIGURATION_PROPERTY_DEVICE_NAME ) )
		|| !( out->m_authenticationUsername = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( file,
			NGHOST_WATCHER_CONFIGURATION_PROPERTY_AUTHENTICATION_USERNAME ) )
		|| !( out->m_authenticationPassword = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( file,
			NGHOST_WATCHER_CONFIGURATION_PROPERTY_AUTHENTICATION_PASSWORD ) )
		|| !( out->m_networkListeningInterface = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( file,
			NGHOST_WATCHER_CONFIGURATION_PROPERTY_NETWORK_LISTENING_INTERFACE ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out->m_authenticationPassword );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_deviceName );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Get values
	out->m_networkListeningPort = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( file,
		NGHOST_WATCHER_CONFIGURATION_PROPERTY_NETWORK_LISTENING_PORT );

	// Build token trusted ip list
	if( !( out->m_authenticationTrustedIPList = NGhostCommon_Token_NGhostTrustedTokenIPList_Build( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( file,
		NGHOST_WATCHER_CONFIGURATION_PROPERTY_AUTHENTICATION_AUTHORIZED_TOKEN_IP ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_authenticationPassword );
		NFREE( out->m_authenticationUsername );
		NFREE( out->m_deviceName );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Destroy file
	NLib_Fichier_Clef_NFichierClef_Detruire( &file );

	// OK
	return out;
}

/**
 * Build default watcher configuration file
 *
 * @return the default watcher configuration
 */
__ALLOC NGhostWatcherConfiguration *NGhostWatcher_Configuration_NGhostWatcherConfiguration_Build2( void )
{
	// Output
	__OUTPUT NGhostWatcherConfiguration *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostWatcherConfiguration ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Duplicate values
	if( !( out->m_authenticationUsername = NLib_Chaine_Dupliquer( NGHOST_WATCHER_CONFIGURATION_PROPERTY_AUTHENTICATION_USERNAME_DEFAULT ) )
		|| !( out->m_authenticationPassword = NLib_Chaine_Dupliquer( NGHOST_WATCHER_CONFIGURATION_PROPERTY_AUTHENTICATION_PASSWORD_DEFAULT ) )
		|| !( out->m_deviceName = NLib_Chaine_Dupliquer( NGHOST_WATCHER_CONFIGURATION_PROPERTY_DEVICE_NAME_DEFAULT ) )
		|| !( out->m_networkListeningInterface = NLib_Chaine_Dupliquer( NGHOST_WATCHER_CONFIGURATION_PROPERTY_NETWORK_LISTENING_INTERFACE_DEFAULT ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Free
		NFREE( out->m_deviceName );
		NFREE( out->m_authenticationPassword );
		NFREE( out->m_authenticationUsername );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Copy data
	out->m_networkListeningPort = NGHOST_WATCHER_CONFIGURATION_PROPERTY_NETWORK_LISTENING_PORT_DEFAULT;

	// Build trusted ip list
	if( !( out->m_authenticationTrustedIPList = NGhostCommon_Token_NGhostTrustedTokenIPList_Build( NGHOST_WATCHER_CONFIGURATION_PROPERTY_AUTHENTICATION_AUTHORIZED_TOKEN_IP_DEFAULT ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out->m_networkListeningInterface );
		NFREE( out->m_deviceName );
		NFREE( out->m_authenticationPassword );
		NFREE( out->m_authenticationUsername );
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy watcher configuration
 *
 * @param this
 * 		This instance
 */
void NGhostWatcher_Configuration_NGhostWatcherConfiguration_Destroy( NGhostWatcherConfiguration **this )
{
	// Destroy ghost token trusted ip list
	NGhostCommon_Token_NGhostTrustedTokenIPList_Destroy( &(*this)->m_authenticationTrustedIPList );

	// Free
	NFREE( (*this)->m_authenticationUsername );
	NFREE( (*this)->m_authenticationPassword );
	NFREE( (*this)->m_deviceName );
	NFREE( (*this)->m_networkListeningInterface );
	NFREE( (*this) );
}

/**
 * Get device name
 *
 * @param this
 * 		This instance
 *
 * @return the device
 */
const char *NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetDeviceName( const NGhostWatcherConfiguration *this )
{
	return this->m_deviceName;
}

/**
 * Get network listening interface
 *
 * @param this
 * 		This instance
 *
 * @return the listening interface
 */
const char *NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetNetworkListeningInterface( const NGhostWatcherConfiguration *this )
{
	return this->m_networkListeningInterface;
}

/**
 * Get network listening port
 *
 * @param this
 * 		This instance
 *
 * @return the listening port
 */
NU32 NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetNetworkListeningPort( const NGhostWatcherConfiguration *this )
{
	return this->m_networkListeningPort;
}

/**
 * Get authentication username
 *
 * @param this
 * 		This instance
 *
 * @return the authentication username
 */
const char *NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetAuthenticationUsername( const NGhostWatcherConfiguration *this )
{
	return this->m_authenticationUsername;
}

/**
 * Get authentication password
 *
 * @param this
 * 		This instance
 *
 * @return the authentication password
 */
const char *NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetAuthenticationPassword( const NGhostWatcherConfiguration *this )
{
	return this->m_authenticationPassword;
}

/**
 * Get token trusted ip list
 *
 * @param this
 * 		This instance
 *
 * @return the token trusted ip list
 */
const NGhostTrustedTokenIPList *NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetAuthenticationTrustedTokenIPList( const NGhostWatcherConfiguration *this )
{
	return this->m_authenticationTrustedIPList;
}