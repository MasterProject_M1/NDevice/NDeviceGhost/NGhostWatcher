#include "../include/NGhostWatcher.h"

// -----------------------------------
// struct NGhostWatcher::NGhostWatcher
// -----------------------------------

/**
 * Build watcher
 *
 * @return the watcher instance
 */
__ALLOC NGhostWatcher *NGhostWatcher_NGhostWatcher_Build( void )
{
	// Output
	__OUTPUT NGhostWatcher *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NGhostWatcher ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Build configuration
	if( !( out->m_configuration = NGhostWatcher_Configuration_NGhostWatcherConfiguration_Build( NGHOST_WATCHER_CONFIGURATION_FILE_PATH ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build event stack
	if( !( out->m_eventList = NGhostCommon_Event_NGhostEventList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy configuration
		NGhostWatcher_Configuration_NGhostWatcherConfiguration_Destroy( &out->m_configuration );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}
	// Build ghost common
	if( !( out->m_common = NGhostCommon_NGhostCommon_Build( NLib_Chaine_Comparer( NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetNetworkListeningInterface( out->m_configuration ),
			"*",
			NTRUE,
			0 ) ?
			NULL
			: NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetNetworkListeningInterface( out->m_configuration ),
		NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetNetworkListeningPort( out->m_configuration ),
		NGhostWatcher_Service_ReceiveCallback,
		"Watchdog",
		out,
		NDEVICE_TYPE_GHOST_WATCHER,
		NTRUE,
		NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetDeviceName( out->m_configuration ),
		NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetAuthenticationUsername( out->m_configuration ),
		NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetAuthenticationPassword( out->m_configuration ),
		NGHOST_VERSION_MAJOR,
		NGHOST_VERSION_MINOR,
		NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetAuthenticationTrustedTokenIPList( out->m_configuration ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NGhostCommon_Event_NGhostEventList_Destroy( &out->m_eventList );
		NGhostWatcher_Configuration_NGhostWatcherConfiguration_Destroy( &out->m_configuration );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// Build monitoring agent
	out->m_isRunning = NTRUE;
	if( !( out->m_monitoring = NGhostCommon_Monitoring_NMonitoringAgent_Build( NGhostCommon_NGhostCommon_GetTokenManager( out->m_common ),
		out,
		&out->m_isRunning,
		NULL ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Destroy
		NGhostCommon_NGhostCommon_Destroy( &out->m_common );
		NGhostCommon_Event_NGhostEventList_Destroy( &out->m_eventList );
		NGhostWatcher_Configuration_NGhostWatcherConfiguration_Destroy( &out->m_configuration );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy watcher
 *
 * @param this
 * 		This instance
 */
void NGhostWatcher_NGhostWatcher_Destroy( NGhostWatcher **this )
{
	// Destroy monitoring
	NGhostCommon_Monitoring_NMonitoringAgent_Destroy( &(*this)->m_monitoring );

	// Destroy common
	NGhostCommon_NGhostCommon_Destroy( &(*this)->m_common );

	// Destroy event list
	NGhostCommon_Event_NGhostEventList_Destroy( &(*this)->m_eventList );

	// Destroy configuration
	NGhostWatcher_Configuration_NGhostWatcherConfiguration_Destroy( &(*this)->m_configuration );

	// Free
	NFREE( (*this) );
}

/**
 * Is running
 *
 * @param this
 * 		This instance
 *
 * @return if watcher is running
 */
NBOOL NGhostWatcher_NGhostWatcher_IsRunning( const NGhostWatcher *this )
{
	return this->m_isRunning;
}

/**
 * Get configuration
 *
 * @param this
 * 		This instance
 *
 * @return the configuration
 */
const NGhostWatcherConfiguration *NGhostWatcher_NGhostWatcher_GetConfiguration( const NGhostWatcher *this )
{
	return this->m_configuration;
}

/**
 * Get ghost common
 *
 * @param this
 * 		This instance
 *
 * @return the ghost common
 */
const NGhostCommon *NGhostWatcher_NGhostWatcher_GetGhostCommon( const NGhostWatcher *this )
{
	return this->m_common;
}

/**
 * Get event list
 *
 * @param this
 * 		This instance
 *
 * @return the event list
 */
NGhostEventList *NGhostWatcher_NGhostWatcher_GetEventList( const NGhostWatcher *this )
{
	return this->m_eventList;
}

/**
 * Get IP
 *
 * @param this
 * 		This instance
 *
 * @return the ip
 */
const char *NGhostWatcher_NGhostWatcher_GetIP( const NGhostWatcher *this )
{
	return NGhostCommon_NGhostCommon_GetIP( this->m_common );
}
