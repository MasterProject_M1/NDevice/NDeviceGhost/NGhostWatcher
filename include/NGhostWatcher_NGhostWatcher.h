#ifndef NGHOSTWATCHER_NGHOSTWATCHER_PROTECT
#define NGHOSTWATCHER_NGHOSTWATCHER_PROTECT

// -----------------------------------
// struct NGhostWatcher::NGhostWatcher
// -----------------------------------

typedef struct NGhostWatcher
{
	// Configuration
	NGhostWatcherConfiguration *m_configuration;

	// Monitoring
	NMonitoringAgent *m_monitoring;

	// Event stack
	NGhostEventList *m_eventList;

	// Ghost common
	NGhostCommon *m_common;

	// Is running?
	NBOOL m_isRunning;
} NGhostWatcher;

/**
 * Build watcher
 *
 * @return the watcher instance
 */
__ALLOC NGhostWatcher *NGhostWatcher_NGhostWatcher_Build( void );

/**
 * Destroy watcher
 *
 * @param this
 * 		This instance
 */
void NGhostWatcher_NGhostWatcher_Destroy( NGhostWatcher** );

/**
 * Is running
 *
 * @param this
 * 		This instance
 *
 * @return if watcher is running
 */
NBOOL NGhostWatcher_NGhostWatcher_IsRunning( const NGhostWatcher* );

/**
 * Get configuration
 *
 * @param this
 * 		This instance
 *
 * @return the configuration
 */
const NGhostWatcherConfiguration *NGhostWatcher_NGhostWatcher_GetConfiguration( const NGhostWatcher* );

/**
 * Get ghost common
 *
 * @param this
 * 		This instance
 *
 * @return the ghost common
 */
const NGhostCommon *NGhostWatcher_NGhostWatcher_GetGhostCommon( const NGhostWatcher* );

/**
 * Get event list
 *
 * @param this
 * 		This instance
 *
 * @return the event list
 */
NGhostEventList *NGhostWatcher_NGhostWatcher_GetEventList( const NGhostWatcher* );

/**
 * Get IP
 *
 * @param this
 * 		This instance
 *
 * @return the ip
 */
const char *NGhostWatcher_NGhostWatcher_GetIP( const NGhostWatcher* );

#endif // !NGHOSTWATCHER_NGHOSTWATCHER_PROTECT
