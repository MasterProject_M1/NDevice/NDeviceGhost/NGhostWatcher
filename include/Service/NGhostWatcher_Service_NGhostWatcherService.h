#include "../../../../../NLib/NHTTP/include/Commun/HTTP/NHTTP_Commun_HTTP.h"

#ifndef NGHOSTWATCHER_SERVICE_NGHOSTWATCHERSERVICE_PROTECT
#define NGHOSTWATCHER_SERVICE_NGHOSTWATCHERSERVICE_PROTECT

// -------------------------------------------------
// enum NGhostWatcher::Service::NGhostWatcherService
// -------------------------------------------------

typedef enum NGhostWatcherService
{
	NGHOST_WATCHER_SERVICE_EVENT_GET,
	NGHOST_WATCHER_SERVICE_EVENT_POST,
	NGHOST_WATCHER_SERVICE_EVENT_DELETE,

	NGHOST_WATCHER_SERVICES
} NGhostWatcherService;

/**
 * Find service
 *
 * @param requestedElement
 * 		The requested element
 * @param cursor
 * 		The cursor in requested element
 * @param requestType
 * 		The request type
 *
 * @return the service type
 */
NGhostWatcherService NGhostWatcher_Service_NGhostWatcherService_FindService( const char *requestedElement,
	NU32 *cursor,
	NTypeRequeteHTTP requestType );

#ifdef NGHOSTWATCHER_SERVICE_NGHOSTWATCHERSERVICE_INTERNE
__PRIVATE const char NGhostWatcherServiceName[ NGHOST_WATCHER_SERVICES ][ 32 ] =
{
	NDEVICE_COMMON_TYPE_GHOST_WATCHER_EVENT_SERVICE,
	NDEVICE_COMMON_TYPE_GHOST_WATCHER_EVENT_SERVICE,
	NDEVICE_COMMON_TYPE_GHOST_WATCHER_EVENT_SERVICE
};

__PRIVATE NTypeRequeteHTTP NGhostWatcherServiceType[ NGHOST_WATCHER_SERVICES ] =
{
	NTYPE_REQUETE_HTTP_GET,
	NTYPE_REQUETE_HTTP_POST,
	NTYPE_REQUETE_HTTP_DELETE
};
#endif // NGHOSTWATCHER_SERVICE_NGHOSTWATCHERSERVICE_INTERNE

#endif // !NGHOSTWATCHER_SERVICE_NGHOSTWATCHERSERVICE_PROTECT
