#ifndef NGHOSTWATCHER_SERVICE_PROTECT
#define NGHOSTWATCHER_SERVICE_PROTECT

// --------------------------------
// namespace NGhostWatcher::Service
// --------------------------------

// enum NGhostWatcher::Service::NGhostWatcherService
#include "NGhostWatcher_Service_NGhostWatcherService.h"

/**
 * Receive callback
 *
 * @param request
 * 		The request
 * @param client
 * 		The requesting client
 *
 * @return the http response
 */
__CALLBACK __ALLOC NReponseHTTP *NGhostWatcher_Service_ReceiveCallback( const NRequeteHTTP *request,
	const NClientServeur *client );

#endif // !NGHOSTWATCHER_SERVICE_PROTECT
