#ifndef NGHOSTWATCHER_PROTECT
#define NGHOSTWATCHER_PROTECT

// -----------------------
// namespace NGhostWatcher
// -----------------------

// namespace NLib
#include "../../../../NLib/NLib/NLib/include/NLib/NLib.h"

// __::Version
#include "__/__NGhostWatcher_Version.h"

// namespace NHTTP
#include "../../../../NLib/NHTTP/include/NHTTP.h"

// namespace NParser
#include "../../../../NParser/NParser/include/NParser.h"

// namespace NDeviceCommon
#include "../../../NDeviceCommon/include/NDeviceCommon.h"

// namespace NGhostCommon
#include "../../NGhostCommon/include/NGhostCommon.h"

// namespace NGhostWatcher::Service
#include "Service/NGhostWatcher_Service.h"

// namespace NGhostWatcher::Configuration
#include "Configuration/NGhostWatcher_Configuration.h"

// struct NGhostWatcher::NGhostWatcher
#include "NGhostWatcher_NGhostWatcher.h"

#endif // !NGHOSTWATCHER_PROTECT
