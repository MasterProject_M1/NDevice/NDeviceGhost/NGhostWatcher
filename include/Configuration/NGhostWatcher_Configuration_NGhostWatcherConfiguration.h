#ifndef NGHOSTWATCHER_CONFIGURATION_NGHOSTWATCHERCONFIGURATION_PROTECT
#define NGHOSTWATCHER_CONFIGURATION_NGHOSTWATCHERCONFIGURATION_PROTECT

// ---------------------------------------------------------------
// struct NGhostWatcher::Configuration::NGhostWatcherConfiguration
// ---------------------------------------------------------------

typedef struct NGhostWatcherConfiguration
{
	/* Device */
	// Device name
	char *m_deviceName;

	/* Network */
	// Listening interface (* = all interface, X.X.X.X means one ip)
	char *m_networkListeningInterface;

	// Listening port
	NU32 m_networkListeningPort;

	/* Authentication */
	// Username
	char *m_authenticationUsername;

	// Password
	char *m_authenticationPassword;

	// Ghost token trusted ip list
	NGhostTrustedTokenIPList *m_authenticationTrustedIPList;
} NGhostWatcherConfiguration;

/**
 * Build watcher configuration
 *
 * @param configurationFilePath
 * 		The configuration file path
 *
 * @return the watcher configuration instance
 */
__ALLOC NGhostWatcherConfiguration *NGhostWatcher_Configuration_NGhostWatcherConfiguration_Build( const char *configurationFilePath );

/**
 * Build default watcher configuration file
 *
 * @return the default watcher configuration
 */
__ALLOC NGhostWatcherConfiguration *NGhostWatcher_Configuration_NGhostWatcherConfiguration_Build2( void );
/**
 * Destroy watcher configuration
 *
 * @param this
 * 		This instance
 */
void NGhostWatcher_Configuration_NGhostWatcherConfiguration_Destroy( NGhostWatcherConfiguration** );

/**
 * Get device name
 *
 * @param this
 * 		This instance
 *
 * @return the device
 */
const char *NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetDeviceName( const NGhostWatcherConfiguration* );

/**
 * Get network listening interface
 *
 * @param this
 * 		This instance
 *
 * @return the listening interface
 */
const char *NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetNetworkListeningInterface( const NGhostWatcherConfiguration* );

/**
 * Get network listening port
 *
 * @param this
 * 		This instance
 *
 * @return the listening port
 */
NU32 NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetNetworkListeningPort( const NGhostWatcherConfiguration* );

/**
 * Get authentication username
 *
 * @param this
 * 		This instance
 *
 * @return the authentication username
 */
const char *NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetAuthenticationUsername( const NGhostWatcherConfiguration* );

/**
 * Get authentication password
 *
 * @param this
 * 		This instance
 *
 * @return the authentication password
 */
const char *NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetAuthenticationPassword( const NGhostWatcherConfiguration* );

/**
 * Get token trusted ip list
 *
 * @param this
 * 		This instance
 *
 * @return the token trusted ip list
 */
const NGhostTrustedTokenIPList *NGhostWatcher_Configuration_NGhostWatcherConfiguration_GetAuthenticationTrustedTokenIPList( const NGhostWatcherConfiguration* );

#endif // !NGHOSTWATCHER_CONFIGURATION_NGHOSTWATCHERCONFIGURATION_PROTECT
