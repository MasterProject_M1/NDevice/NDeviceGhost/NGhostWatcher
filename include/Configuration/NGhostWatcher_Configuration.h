#ifndef NGHOSTWATCHER_CONFIGURATION_PROTECT
#define NGHOSTWATCHER_CONFIGURATION_PROTECT

// --------------------------------------
// namespace NGhostWatcher::Configuration
// --------------------------------------

// enum NGhostWatcher::Configuration::NGhostWatcherConfigurationProperty
#include "NGhostWatcher_Configuration_NGhostWatcherConfigurationProperty.h"

// struct NGhostWatcher::Configuration::NGhostWatcherConfiguration
#include "NGhostWatcher_Configuration_NGhostWatcherConfiguration.h"

// Configuration file path
#define NGHOST_WATCHER_CONFIGURATION_FILE_PATH		"GhostWatcher.conf"

#endif // !NGHOSTWATCHER_CONFIGURATION_PROTECT
