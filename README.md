NGhostWatcher
=============

Introduction
------------

This project is the event stack of the ghost architecture.

Here you will find all the event added by the services that are aware
of ghost watcher hostname/ip.

When you add an action to the event list, here is the sub-process which occurs:

- Your action is based on hash provided by service which generate it
- We generate a hash, based on the action hash AND a time frame:
  
  To prevent two exact same actions from being pushed at the same time by
  two different services, we added a time frame during which two same event
  would generate the same hash.
  
  The time frame is calculated this way:
  
  ```c
  currentTime = GetTicks( );
  currentTime = currentTime - ( currentTime % TIME_FRAME_MS );
  ```

Goal
----

The goal is to provide an event-source. These events will then be extracted
by the IA, which will execute it, and extract habits from it.

Event structure
---------------

An event structure looks like this:

```json
{
	"eventHash": "Base64EventHash",
	"timestamp": 123456789,
	"delayBeforeActivation": 0,
	"isReady": true,
	"actionHash": "Base64ActionHash",
	"source":
	{
		"ip": "SOURCE_IP",
		"type": "DeviceTypeName",
		"uuid": "DeviceUUID"
	},
	"destination":
	{
		"hostname": "DESTINATION_HOSTNAME",
		"port": 12345
	},
	"parameter": [ "Argument", "List" ]
}
```

Please note the delayBeforeActivation, which is expresses in milliseconds, and the
isReady key which is related to delay and display if time has elapsed or not.

Examples
--------

> Add an event (contact google.fr:12345 and execute action with %MDRTEST% hash)

```bash
curl test:test@127.0.0.1:16563/ghost/event/ -X POST -d "{ \"actionHash\": \"MDRTEST\", \"source\": { \"type\": \"GhostClient\", \"uuid\": \"BAD_UUID\" }, \"destination\": { \"hostname\": \"google.fr\", \"port\": 12345 }, \"parameter\": [ 1, 2 ] }"
```

Will add this to stack

```json
"9z_dtsWun0QPkn70qCCjyQ==": {
	"eventHash": "9z_dtsWun0QPkn70qCCjyQ==",
	"timestamp": 1529363562,
	"actionHash": "MDRTEST",
	"destination": {
		"hostname": "google.fr",
		"port": 12345
	},
	"source": {
		"ip": "127.0.0.1",
		"type": "GhostClient",
		"uuid": "BAD_UUID"
	},
	"parameter": [
		1,
		2
	]
}
```

> Get an event (from hash only)

```bash
curl test:test@127.0.0.1:16563/ghost/event/9z_dtsWun0QPkn70qCCjyQ==
```

Will produce after the previous add

```json
{
	"eventHash": "9z_dtsWun0QPkn70qCCjyQ==",
	"timestamp": 1529363562,
	"delayBeforeActivation": 0,
	"isReady": true,
	"actionHash": "MDRTEST",
	"destination": {
		"hostname": "google.fr",
		"port": 12345
	},
	"source": {
		"ip": "127.0.0.1",
		"type": "GhostClient",
		"uuid": "BAD_UUID"
	},
	"parameter": [
		1,
		2
	]
}
```

> Delete this event

```bash
curl test:test@127.0.0.1:16563/ghost/event/9z_dtsWun0QPkn70qCCjyQ== -X DELETE
```

> Flush all the events

```bash
curl test:test@127.0.0.1:16563/ghost/event/ -X DELETE
```

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/MasterProject_M1/NDevice/NDeviceGhost/NGhostWatcher.git
